<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Valkyr\CriteriaBuilder\Services\Laravel\SearchCriteriaAwareTrait;

class Supplier extends Model
{
    use SearchCriteriaAwareTrait;
    use HasFactory;

    public const API_FILTER_MAPPER = [
        'supplier_id' => 'supplierId'
    ];

    public $incrementing = false;
    protected $keyType = 'uuid';

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function brands(): HasMany
    {
        return $this->hasMany(Brand::class);
    }
}
