<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Valkyr\CriteriaBuilder\Services\Laravel\SearchCriteriaAwareTrait;

class Brand extends Model
{
    use SearchCriteriaAwareTrait;
    use HasFactory;

    public const API_FILTER_MAPPER = [
        'id' => 'id',
        'name' => 'name'
    ];

    public $incrementing = false;
    protected $keyType = 'uuid';

    public function supplier(): BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }
}
