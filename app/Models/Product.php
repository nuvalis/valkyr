<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Valkyr\CriteriaBuilder\Services\Laravel\SearchCriteriaAwareTrait;

class Product extends Model
{
    use SearchCriteriaAwareTrait;
    use HasFactory;

    public const API_FILTER_MAPPER = [
        'id' => 'id',
        'name' => 'name',
        'sku' => 'sku',
        'description' => 'description',
        'brand_id' => 'brandId',
        'supplier_id' => 'supplierId',
        'created_at' => 'createdAt',
        'updated_at' => 'updatedAt',
        'supplier_article_number' => 'supplierArticleNumber'
    ];

    public $incrementing = false;
    protected $keyType = 'uuid';

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    /**
     * @return BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class);
    }
}
