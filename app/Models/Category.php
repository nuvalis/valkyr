<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Valkyr\CriteriaBuilder\Services\Laravel\SearchCriteriaAwareTrait;

class Category extends Model
{
    use SearchCriteriaAwareTrait;
    use HasFactory;

    public const API_FILTER_MAPPER = [
        'id' => 'id',
        'name' => 'name',
        'parent_id' => 'parentId',
        'created_at' => 'createdAt',
        'updated_at' => 'updatedAt',
        'page_id' => 'pageId',
        'url_key' => 'urlKey'
    ];

    public $incrementing = false;
    protected $keyType = 'uuid';

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }
}
