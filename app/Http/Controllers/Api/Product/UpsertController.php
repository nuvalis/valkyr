<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class UpsertController extends Controller
{
    /**
     * @param Request $request
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'products.*.sku' => 'sometimes|required',
            'products.*.description' => 'sometimes|required',
            'products.*.name' => 'sometimes|required',
            'products.*.brandId' => 'sometimes|required|uuid|exists:brands,id',
            'products.*.supplierId' => 'sometimes|required|uuid|exists:suppliers,id',
        ]);

        $products = $request->input('products');
        $filteredProducts = [];
        $model = new Product();
        foreach ($products as $index => $product) {
            foreach ($product as $key => $value) {
                $filteredProducts[$index][$this->convertFieldAliasToColumn($model, $key)] = $value;
            }
        }

        try {
            return Product::upsert($filteredProducts, ['sku']);
        } catch (QueryException $exception) {
            return $exception->getMessage();
        }
    }

    private function convertFieldAliasToColumn(Model $model, string $string)
    {
        $alias = array_flip($model::API_FILTER_MAPPER)[$string] ?? null;

        if ($alias) {
            return $alias;
        }

        if (!array_key_exists($string, $model::API_FILTER_MAPPER)) {
            throw new Exception(sprintf(
                'Column/Key: "%s", on constant $model::API_FILTER_MAPPER is not available on this model: %s,
                            Expected:> $model::API_FILTER_MAPPER = [ "original_column" => "aliasColumn" ]',
                $string,
                $model::class
            ));
        }

        return $string;
    }
}
