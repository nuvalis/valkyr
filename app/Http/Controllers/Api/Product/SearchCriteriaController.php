<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SearchCriteriaController extends Controller
{
    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws Exception
     */
    public function __invoke(Request $request): AnonymousResourceCollection
    {
        return Product::handleSearchCriteriaRequestWithResource($request);
    }
}
