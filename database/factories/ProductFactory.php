<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    public function definition(): array
    {
        $name = $this->faker->words(3, true);
        return [
            'id' => Str::uuid()->toString(),
            'name' => $name,
            'sku' => Str::slug(sprintf('%s-%s', $name, $this->faker->randomNumber())),
            'description' => $this->faker->realText()
        ];
    }
}
