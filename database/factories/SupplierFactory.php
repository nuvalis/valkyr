<?php

namespace Database\Factories;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SupplierFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Supplier::class;

    public function definition(): array
    {
        $name = $this->faker->company;
        return [
            'id' => Str::uuid()->toString(),
            'name' => $name,
        ];
    }
}
