<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    public function definition(): array
    {
        $name = $this->faker->name;
        return [
            'id' => Str::uuid()->toString(),
            'name' => $name,
            'url_key' => Str::slug($name),
            'description' => $this->faker->realText()
        ];
    }
}
