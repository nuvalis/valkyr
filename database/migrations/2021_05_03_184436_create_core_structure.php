<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCoreStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'currencies',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->string('code', 3);
                $table->string('symbol', 5);
            }
        );

        Schema::create(
            'countries',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->string('iso', 2);
                $table->string('iso3', 3);
                $table->string('name', 5);
                $table->smallInteger('numeric_code');
                $table->smallInteger('phone_code');
            }
        );

        Schema::create(
            'pages',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->jsonb('data')->nullable();
                $table->text('slug')->unique();
                $table->timestamps();
            }
        );

        Schema::create(
            'categories',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->uuid('parent_id')->nullable();
                $table->text('url_key')->unique();
                $table->foreignUuid('page_id')->nullable()->constrained();
                $table->text('name');
                $table->text('description');
                $table->timestamps();
            }
        );

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('categories');
        });

        Schema::create(
            'tags',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->text('label');
                $table->timestamps();
            }
        );

        Schema::create(
            'suppliers',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->string('name');
                $table->timestamps();
            }
        );

        Schema::create(
            'brands',
            function (Blueprint $table) {
                $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
                $table->foreignUuid('supplier_id')->nullable()->constrained();
                $table->string('name');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('tags');
        Schema::dropIfExists('brands');
        Schema::dropIfExists('suppliers');
        Schema::dropIfExists('pages');
    }
}
