<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique()->default(DB::raw('uuid_generate_v4()'));
            $table->foreignUuid('supplier_id')->nullable()->constrained();
            $table->foreignUuid('brand_id')->nullable()->constrained();
            $table->text('name');
            $table->text('description');
            $table->text('sku')->unique();
            $table->text('ean')->nullable();
            $table->jsonb('attributes')->nullable();
            $table->text('supplier_article_number')->nullable();
            $table->enum('status', [
                    'new',
                    'in_progress',
                    'published',
                    'phased_out',
                    'supplier_phased_out'
                ]
            )->default('new');
            $table->timestamps();
        });

        Schema::create('product_category', function (Blueprint $table) {
            $table->foreignUuid('product_id')->constrained()->onDelete('cascade');
            $table->foreignUuid('category_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
        Schema::dropIfExists('products');
    }
}
