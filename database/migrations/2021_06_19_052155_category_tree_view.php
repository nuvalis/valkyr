<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CategoryTreeView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::unprepared("
            -- Create view with Category Paths
            CREATE RECURSIVE VIEW category_tree_view (
                 tree_id,
                 parent_id,
                 name,
                 category_tree,
                 category_path,
                 url_path,
                 depth
            ) AS ( SELECT id,
                   parent_id,
                   name,
                   name                 AS category_tree,
                   id::text             AS category_path,
                   url_key              AS url_path,
                   0                    AS depth
            FROM categories
            WHERE parent_id IS NULL
            UNION ALL
            SELECT c.id,
                   c.parent_id,
                   c.name,
                   lower(category_tree_view.category_tree)   || '/' || c.name      AS category_tree,
                   category_tree_view.category_path          || '/' || c.id::text           AS category_path,
                   category_tree_view.url_path               || '/' || c.url_key            AS url_path,
                   depth + 1                                                                AS depth
            FROM category_tree_view JOIN categories c ON category_tree_view.tree_id = c.parent_id);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::unprepared('DROP VIEW category_tree_view;');
    }
}
