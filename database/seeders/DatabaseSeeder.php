<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $productFactory = Product::factory()->count(100);
        $level2 = Category::factory(3)->has($productFactory, 'products');
        $level1 = Category::factory()->count(3)->has($level2, 'children');
        Category::factory(10)->has($level1, 'children')->create();
        Supplier::factory(10)->has(Brand::factory(2))->create();

        $brands = Brand::all();
        $categories = Category::all();
        $all = Product::all();

        foreach ($all as $product) {
            $product->brand_id = $brands->random(1)->first()->getAttribute('id');
            $ids = $categories->random(4)->pluck('id')->values()->toArray();
            $product->categories()->attach($ids);
            $product->save();
        }
    }
}
